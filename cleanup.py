from os.path import dirname, join, realpath, exists
from os import listdir, system

script_dir = dirname(realpath(__file__))

# For every subdir of v1/, check if it contains both an app.yml and app.yml.jinja
# If it does, delete app.yml and only keep app.yml.jinja
for subdir in listdir(join(script_dir, "v1")):
    yml = join(script_dir, "v1", subdir, "app.yml")
    jinja = join(script_dir, "v1", subdir, "app.yml.jinja")
    if exists(yml) and exists(jinja):
        print(f"Cleaning {subdir}")
        system(f"rm {yml}")


