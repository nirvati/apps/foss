from os.path import dirname, join, realpath, exists
from os import listdir, system

script_dir = dirname(realpath(__file__))

js_builder = join(script_dir, "..", "..", "js-builder")

# For every subdir of v1/, check if it has a _tera/_src dir
# If it does, run "deno run -A js_builder/build.ts <path to _src dir> <path to _tera dir>"
for subdir in listdir(join(script_dir, "v1")):
    tera_dir = join(script_dir, "v1", subdir, "_tera")
    src_dir = join(tera_dir, "_src")
    if exists(tera_dir) and exists(src_dir):
        print(f"Building {subdir}")
        system(f"deno run -A {js_builder}/build.ts {src_dir} {tera_dir}")

